﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TP0v2.Startup))]
namespace TP0v2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
