
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 02/11/2022 16:34:03
-- Generated from EDMX file: D:\KOUASSI\Projets\Learn\TP0v2\TP0v2\DBAL\Entities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [TP0];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Regions'
CREATE TABLE [dbo].[Regions] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'Etablissements'
CREATE TABLE [dbo].[Etablissements] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Nom] nvarchar(128)  NOT NULL,
    [Effectif] smallint  NOT NULL,
    [RegionID] int  NOT NULL
);
GO

-- Creating table 'Infrastructures'
CREATE TABLE [dbo].[Infrastructures] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Libelle] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'EtablissementInfrastructure'
CREATE TABLE [dbo].[EtablissementInfrastructure] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [EtablissementID] int  NOT NULL,
    [Nombre] smallint  NOT NULL,
    [InfrastructureID] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [ID] in table 'Regions'
ALTER TABLE [dbo].[Regions]
ADD CONSTRAINT [PK_Regions]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Etablissements'
ALTER TABLE [dbo].[Etablissements]
ADD CONSTRAINT [PK_Etablissements]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'Infrastructures'
ALTER TABLE [dbo].[Infrastructures]
ADD CONSTRAINT [PK_Infrastructures]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [ID] in table 'EtablissementInfrastructure'
ALTER TABLE [dbo].[EtablissementInfrastructure]
ADD CONSTRAINT [PK_EtablissementInfrastructure]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [RegionID] in table 'Etablissements'
ALTER TABLE [dbo].[Etablissements]
ADD CONSTRAINT [FK_RegionEtablissement]
    FOREIGN KEY ([RegionID])
    REFERENCES [dbo].[Regions]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_RegionEtablissement'
CREATE INDEX [IX_FK_RegionEtablissement]
ON [dbo].[Etablissements]
    ([RegionID]);
GO

-- Creating foreign key on [EtablissementID] in table 'EtablissementInfrastructure'
ALTER TABLE [dbo].[EtablissementInfrastructure]
ADD CONSTRAINT [FK_EtablissementPivotEI]
    FOREIGN KEY ([EtablissementID])
    REFERENCES [dbo].[Etablissements]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EtablissementPivotEI'
CREATE INDEX [IX_FK_EtablissementPivotEI]
ON [dbo].[EtablissementInfrastructure]
    ([EtablissementID]);
GO

-- Creating foreign key on [InfrastructureID] in table 'EtablissementInfrastructure'
ALTER TABLE [dbo].[EtablissementInfrastructure]
ADD CONSTRAINT [FK_InfrastructurePivotEI]
    FOREIGN KEY ([InfrastructureID])
    REFERENCES [dbo].[Infrastructures]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_InfrastructurePivotEI'
CREATE INDEX [IX_FK_InfrastructurePivotEI]
ON [dbo].[EtablissementInfrastructure]
    ([InfrastructureID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------