﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP0v2.DBAL;
using TP0v2.Models;

namespace TP0.Controllers
{
    public class EtablissementController : Controller
    {
        private ApplicationDbContext DB = ApplicationDbContext.Create();

        // GET: Region
        public ActionResult Index()
        {
            try
            {
                List<Etablissement> etablissements = DB.Etablissements
                                         .Include("EtsInfPivots")
                                         .Include("EtsInfPivots.Infrastructure")
                                         .OrderBy(e => e.ID)
                                         .ToList();
                return View(etablissements);
            }
            catch
            {
                return View(new List<Etablissement>());
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Etablissement EC)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Create");
            }

            Etablissement ets = new Etablissement
            {
                Nom = EC.Nom,
                Effectif = EC.Effectif,
                RegionID = EC.RegionID
            };

            DB.Etablissements.Add(ets);
            DB.SaveChanges();
            return RedirectToAction("Index", "Etablissement");
        }


        [HttpGet]
        public ActionResult Edit(int ID)
        {
            Etablissement entity = DB.Etablissements.Find(ID);
            if (entity == null)
            {
                return new HttpNotFoundResult();
            }
            IEnumerable<Region> regions = DB.Regions
                                         .OrderBy(e => e.Nom)
                                         .ToList();
            ViewBag.Regions = regions;
            IEnumerable<Infrastructure> infrastructures = DB.Infrastructures
                                        // .Include("EtsInfPivots")
                                         .OrderBy(e => e.Libelle)
                                         .ToList();
            ViewBag.Infrastructures = infrastructures;
            return View(entity);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Edit(Etablissement ET)
        {
            Etablissement entity = DB.Etablissements.Find(ET.ID);
            if (entity == null)
            {
                return "Etablissement introuvable";
            }
            DbEntityEntry<Etablissement> entry = DB.Entry(entity);
            entry.State = System.Data.Entity.EntityState.Modified;
            Etablissement ets = new Etablissement
            {
                Nom = ET.Nom,
                Effectif = ET.Effectif,
                RegionID = ET.RegionID
            };
            ets.ID = ET.ID;
            entry.CurrentValues.SetValues(ets);
            DB.SaveChanges();
            return "Ok";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Delete(int ID)
        {
            Etablissement e = DB.Etablissements.Find(ID);
            if (e == null)
            {
                return "Etablissement introuvable";
            }
            DB.Etablissements.Remove(e);
            DB.SaveChanges();
            return "Ok";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddInfrastructure(EtsInfPivot input)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Index");
            }

            Etablissement etsEntity = DB.Etablissements.Find(input.EtablissementID);
            if (etsEntity == null)
            {
                return HttpNotFound("Etablissement introuvable");
            }
            Infrastructure infEntity = DB.Infrastructures.Find(input.InfrastructureID);
            if (infEntity == null)
            {
                return HttpNotFound("Infrastructure introuvable");
            }

            EtsInfPivot pivot = new EtsInfPivot
            {
                EtablissementID = etsEntity.ID,
                InfrastructureID = infEntity.ID,
                Nombre = input.Nombre
            };

            DB.EtsInfPivots.Add(pivot);
            DB.SaveChanges();
            return RedirectToAction("Index", "Etablissement");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string RemoveInfrastructure(int ID)
        {
            EtsInfPivot e = DB.EtsInfPivots.Find(ID);
            if (e == null)
            {
                return "Entrée introuvable";
            }
            DB.EtsInfPivots.Remove(e);
            DB.SaveChanges();
            return "Ok";
        }
    }
}