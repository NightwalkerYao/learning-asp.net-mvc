﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP0v2.DBAL;
using TP0v2.Models;

namespace TP0.Controllers
{
    public class RegionController : Controller
    {
        private ApplicationDbContext DB = ApplicationDbContext.Create();

        // GET: Region
        public ActionResult Index()
        {
            try
            {
                //on saute un certain nombre d'article et on en prend la quantité voulue
                List<Region> regions = DB.Regions
                                         .OrderBy(a => a.ID)
                                         .ToList();
                return View(regions);
            }
            catch
            {
                return View(new List<Region>());
            }
        }

        [HttpGet]
        public ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Region RC)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Create");
            }

            Region region = new Region
            {
                Nom = RC.Nom
            };

            DB.Regions.Add(region);
            DB.SaveChanges();
            return RedirectToAction("Index", "Region");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Edit(Region RE)
        {
            Region entity = DB.Regions.Find(RE.ID);
            if (entity == null)
            {
                return "Région introuvable";
            }
            DbEntityEntry<Region> entry = DB.Entry(entity);
            entry.State = System.Data.Entity.EntityState.Modified;
            Region region = new Region
            {
                Nom = RE.Nom,
            };
            region.ID = RE.ID;
            entry.CurrentValues.SetValues(region);
            DB.SaveChanges();
            return "Ok";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Delete(int ID)
        {
            Region r = DB.Regions.Find(ID);
            if (r == null)
            {
                return "Région introuvable";
            }
            DB.Regions.Remove(r);
            DB.SaveChanges();
            return "Ok";
        }
    }
}