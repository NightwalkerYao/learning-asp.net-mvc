﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP0v2.DBAL;
using TP0v2.Models;

namespace TP0.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext DB = ApplicationDbContext.Create();

        public ActionResult Index(int RegionID = 1)
        {
            dynamic Dashboard = new ExpandoObject();
            Dashboard.Region = DB.Regions.Find(RegionID);
            Dashboard.Etablissements = GetEtablissements(RegionID);
            List<int> EtablissementsIDs = new List<int>();
            foreach (Etablissement et in Dashboard.Etablissements)
            {
                EtablissementsIDs.Add(et.ID);
            }
            Dashboard.Pivots = DB.EtsInfPivots
                                                .Include("Infrastructure")
                                                .Include("Etablissement")
                                                .Where(i => EtablissementsIDs.Contains(i.EtablissementID))
                                                .ToList();
            return View(Dashboard);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        private List<Etablissement> GetEtablissements(int RegionID)
        {
            List<Etablissement> etablissements = DB.Etablissements
                                         .Where(e => e.RegionID == RegionID)
                                         .OrderBy(e => e.ID)
                                         .ToList();
            return etablissements;
        }
    }
}