﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TP0v2.DBAL;
using TP0v2.Models;

namespace TP0.Controllers
{
    public class InfrastructureController : Controller
    {
        private ApplicationDbContext DB = ApplicationDbContext.Create();

        // GET: Region
        public ActionResult Index()
        {
            try
            {
                //on saute un certain nombre d'article et on en prend la quantité voulue
                List<Infrastructure> infras = DB.Infrastructures
                                         .Include("EtsInfPivots")
                                         .Include("EtsInfPivots.Etablissement")
                                         .OrderBy(e => e.ID)
                                         .ToList();
                return View(infras);
            }
            catch
            {
                return View(new List<Infrastructure>());
            }
        }


        [HttpGet]
        public ActionResult Create()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Infrastructure IC)
        {
            if (!ModelState.IsValid)
            {
                return RedirectToAction("Create");
            }

            Infrastructure infra = new Infrastructure
            {
                Libelle = IC.Libelle
            };

            DB.Infrastructures.Add(infra);
            DB.SaveChanges();
            return RedirectToAction("Index", "Infrastructure");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Edit(Infrastructure Input)
        {
            Infrastructure entity = DB.Infrastructures.Find(Input.ID);
            if (entity == null)
            {
                return "Infrastructure introuvable";
            }
            System.Data.Entity.Infrastructure.DbEntityEntry<Infrastructure> entry = DB.Entry(entity);
            entry.State = System.Data.Entity.EntityState.Modified;
            Infrastructure infra = new Infrastructure
            {
                Libelle = Input.Libelle
            };
            infra.ID = Input.ID;
            entry.CurrentValues.SetValues(infra);
            DB.SaveChanges();
            return "Ok";
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public string Delete(int ID)
        {
            Infrastructure i = DB.Infrastructures.Find(ID);
            if (i == null)
            {
                return "Infrastructure introuvable";
            }
            DB.Infrastructures.Remove(i);
            DB.SaveChanges();
            return "Ok";
        }
    }
}